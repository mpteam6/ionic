import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = environment.apiUrl;
const API_KEY = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  categories: any;
  category: any;
  categoryTitle: any;
  article: any;
  title: any;

  constructor(private http: HttpClient) { }

  getData(url): Observable<any> {
    var address = `${API_URL}/${url}?&format=json&show-fields=all&page-size=50&api-key=${API_KEY}`;
    console.log(address);
    return this.http.get(address);
  }

}
