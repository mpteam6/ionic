import { CategoriesService } from './../categories.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-saved-articles',
  templateUrl: './saved-articles.page.html',
  styleUrls: ['./saved-articles.page.scss'],
})
export class SavedArticlesPage implements OnInit {

  key1 = 'ArticleTitle';
  key2 = 'ArticleImage';
  key3 = 'ArticleContent';

  title: any = [];
  image: any = [];
  content: any = [];

  constructor(
    private categoriesService: CategoriesService,
    private router: Router,
    public storage: Storage
    ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.storage.get(this.key1).then(data => {
      this.title.length = 0;
      this.title = data;
    });
    this.storage.get(this.key2).then(data => {
      this.image.length = 0;
      this.image = data;
    });
    this.storage.get(this.key3).then(data => {
      this.content.length = 0;
      this.content = data;
    });
  }

  home() {
    this.router.navigate(['/home']);
  }

  openArticle(title, content) {
    this.categoriesService.article = content;
    this.categoriesService.title = title;
    this.categoriesService.categoryTitle = 'Saved Articles';
    this.router.navigate(['/article']);
  }

  delete(i) {
    this.title.splice(i, 1);
    this.image.splice(i, 1),
    this.content.splice(i, 1);
    this.storage.set(this.key1, this.title);
    this.storage.set(this.key2, this.image);
    this.storage.set(this.key3, this.content);
  }

}
