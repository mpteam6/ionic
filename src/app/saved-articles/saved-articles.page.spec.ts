import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedArticlesPage } from './saved-articles.page';

describe('SavedArticlesPage', () => {
  let component: SavedArticlesPage;
  let fixture: ComponentFixture<SavedArticlesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedArticlesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedArticlesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
