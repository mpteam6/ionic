import { CategoriesService } from './../categories.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { IonContent } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  items: any;
  categoryList: any = [];

  input: string;
  val: any;

  show: string = "all";
  showText: string ="Show Favorites";

  webTitle: any = [];
  id: any = [];
  webTitleFull: any = [];
  idFull: any = [];

  favId: any = [];
  favIdFull: any = [];
  favTitle: any = [];
  favTitleFull: any = [];

  constructor(
    private categoriesService: CategoriesService,
    private router: Router,
    public storage: Storage) {
    }

  ngOnInit() {
     this.categoriesService
      .getData('sections')
      .subscribe(data => {
        if (data) {
        this.items = data;
        } else {
          alert("No data available. Check your connetion.")
        }
        // Fill categoryList with category titles
        for (let i = 0; i < this.items.response.results.length; i++) {
          this.categoryList[i] = this.items.response.results[i];
          if (this.categoryList[i].id == "better-business" ||
            this.categoryList[i].id == "cardiff" ||
            this.categoryList[i].id == "culture-network" ||
            this.categoryList[i].id == "crosswords" ||
            this.categoryList[i].id == "enterprise-network" ||
            this.categoryList[i].id == "extra" ||
            this.categoryList[i].id == "government-computing-network" ||
            this.categoryList[i].id == "guardian-professional" ||
            this.categoryList[i].id == "jobsadvice" ||
            this.categoryList[i].id == "katine" ||
            this.categoryList[i].id == "leeds" ||
            this.categoryList[i].id == "local" ||
            this.categoryList[i].id == "local-government-network" ||
            this.categoryList[i].id == "social-enterprise-network" ||
            this.categoryList[i].id == "travel/offers") {
          } else {
              // Push to arrays each title and id
              this.webTitle.push(this.categoryList[i].webTitle);
              this.id.push(this.categoryList[i].id);
          }
        }
        // Save full list & remove about
        this.webTitle.splice(0, 1);
        this.id.splice(0, 1);
        this.webTitleFull = this.webTitle;
        this.idFull = this.id;
      });
      // Get favorites
      this.storage.forEach((value, key, index) => {
        if (key == 'ArticleTitle' || key == 'ArticleContent' || key == 'ArticleImage') {
          //console.log("Caught");
        } else {
          this.favTitle.push(key);
          this.favId.push(value);
        }
      });
      this.favTitleFull = this.favTitle;
      this.favIdFull = this.favId;
    }

    favoriteCategory(title) {
      let key = title;
      let value = this.id[this.webTitleFull.indexOf(title)];
      this.storage.get(key).then(data => {
          if (data) {
            alert("Error has happened at line 95");
          } else {
            this.storage.set(key, value);
            this.favTitle.push(key);
            this.favId.push(value);
          }
        });
        this.favTitleFull = this.favTitle;
        this.favIdFull = this.favId;
    }
    
    unfavoriteCategory(title) {
      this.storage.remove(title);
      const pos = this.favTitle.indexOf(title);
      this.favTitle.splice(pos, 1);
      this.favId.splice(pos, 1);
      this.favTitleFull = this.favTitle;
      this.favIdFull = this.favId;
    }
    
    getAllFavoriteCategories() {
      console.log(this.favTitleFull);
      //console.log(this.favIdFull);
    }

    clearAllFavoriteCategories() {
      this.storage.clear();
      this.favId.length = 0;
      this.favTitle.length = 0;
    }

    // Scroll to top of page
    scrollToTop() {
      this.content.scrollToTop(1500);
    }

   scroll(ev: any) {
      //console.log("woo");
   }

    // Search filter for categories
    filterItems(ev: any) {
      this.webTitle = this.webTitleFull;
      this.favTitle = this.favTitleFull;
      const val = ev.target.value;
      if (val && val.trim() !== '') {
        this.webTitle = this.webTitle.filter((item) => {
          return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        this.favTitle = this.favTitle.filter((item) => {
          return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      }}

    // Open selected category and navigate to category page
    // Throws category url to service
    openCategory(title) {
      this.categoriesService.category = this.id[this.webTitleFull.indexOf(title)];
      this.categoriesService.categoryTitle = title;
      this.router.navigate(['/category']);
    }

    // Toggle between showing all categories and favorites
    toggleShow() {
      this.favId.sort();
      this.favTitle.sort();
      if (this.showText === 'Show All') {
        this.showText = 'Show Favorites';
      } else if (this.showText === 'Show Favorites') {
        this.showText = 'Show All';
      }
    }

    isFav(title) {
    if (this.favTitle.indexOf(title) >= 0) {
      return 1;
    } else {
      return 0;
    }
  }

  saved() {
    this.router.navigate(['/saved-articles']);
  }

}
