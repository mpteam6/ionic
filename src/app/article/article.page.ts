import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../categories.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

  categoryTitle: any;
  article: any;
  title: any;
  items: any;

  constructor(
    private categoriesService: CategoriesService,
    private router: Router
    ) { }

  // Get category, article content and title from service
  ngOnInit() {
    this.categoryTitle = this.categoriesService.categoryTitle;
    this.article = this.categoriesService.article;
    this.title = this.categoriesService.title;
  }

  home() {
    this.router.navigate(['/home']);
    }
}
