import { CategoriesService } from './../categories.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  category: any;
  categoryTitle: any;
  items: any;

  articleTitle: any = [];
  articleImage: any = [];
  articleContent: any = [];

  key1 = 'ArticleTitle';
  key2 = 'ArticleImage';
  key3 = 'ArticleContent';

  constructor(
    private categoriesService: CategoriesService,
    private router: Router,
    public storage: Storage,
    private alertController: AlertController
    ) { }

  // Get category id (url) from service, throw it back to get data
  ngOnInit() {
    this.category = this.categoriesService.category;
    this.categoryTitle = this.categoriesService.categoryTitle;
    this.categoriesService
      .getData(this.category)
      .subscribe(data => {
        this.items = data;
      });
  }

  // Get saved artcles 
  ionViewWillEnter(){
    this.storage.get(this.key1).then(data => {
      if (data) {
      this.articleTitle.length = 0;
      this.articleTitle = data;
      }
    });
    this.storage.get(this.key2).then(data => {
      if (data) {
      this.articleImage.length = 0;
      this.articleImage = data;
      }
    });
    this.storage.get(this.key3).then(data => {
      if (data) {
      this.articleContent.length = 0;
      this.articleContent = data;
      }
    });
  }

  // Open article page, throw article content and title to service
  openArticle(article, title) {
    this.categoriesService.article = article;
    this.categoriesService.title = title;
    this.router.navigate(['/article']);
  }

  home() {
    this.router.navigate(['/home']);
  }

  delete () {
    this.storage.clear();
    this.articleTitle.length = 0;
    this.articleImage.length = 0;
    this.articleContent.length = 0;
  }

  save(article) {
    if (this.articleTitle.length >= 10) {
      alert("Maximum amount of articles saved Click here to buy our paid version");
    } else {
      let title = article.webTitle;
      let image = article.fields.thumbnail;
      if (!image) {
        image = 'assets/imgs/500px-The_Guardian_2018.png';
      }
      let content = article.fields.body;

      this.articleTitle.push(title);
      this.articleImage.push(image);
      this.articleContent.push(content);
      this.saveStorage();
    }
  }

  saveStorage() {
    this.storage.set(this.key1, this.articleTitle);
    this.storage.set(this.key2, this.articleImage);
    this.storage.set(this.key3, this.articleContent);
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Article saved successfully!',
      buttons: [{
        text: 'Go to Saved Articles',
        handler: () => {
          //console.log("button 1 works");
          this.router.navigate(['/saved-articles']);
        }
      }, {
        text: 'Ok',
        handler: () => {
          //console.log("button 2 works");
        }
        }
      ]
    });
    await alert.present();
  }

}
